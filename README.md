# Docker container for running c exercism exercises

This project builds a docker container which allows user to download, execute, and submit c exercism exercises.

## Build

Image can be built with the exercism token supplied via build-arg.

Otherwise, the token can be set by env value.
