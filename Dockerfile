FROM debian:bullseye

RUN apt-get update && apt-get install -y wget
WORKDIR /tmp/
RUN wget https://github.com/exercism/cli/releases/download/v3.0.13/exercism-3.0.13-linux-x86_64.tar.gz && \
    tar xzf exercism-3.0.13-linux-x86_64.tar.gz && \
    mv exercism /usr/local/bin
ARG EXERCISM_TOKEN=""
ENV EXERCISM_TOKEN=$EXERCISM_TOKEN

# TODO: change this to entrypoint shell script so that it is usable via docker run supplied token
RUN /bin/bash -c 'exercism configure -w /exercism -t $EXERCISM_TOKEN 2> /dev/null'

ENTRYPOINT ["/usr/local/bin/exercism"]

# for c
# RUN apt-get update && apt-get install -y build-essential man-db man-pages wget 
